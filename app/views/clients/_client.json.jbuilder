json.extract! client, :id, :name, :CPF, :created_at, :updated_at
json.url client_url(client, format: :json)
