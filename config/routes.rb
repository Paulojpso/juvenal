Rails.application.routes.draw do
  resources :reservations
  resources :clients
  resources :librarians
  resources :books
  resources :authors
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
